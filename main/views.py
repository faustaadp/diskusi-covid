from django.shortcuts import render, redirect
from .models import Saran, Like
from django.http import HttpResponse
from .forms import FormSaran
from django.template.loader import render_to_string
from django.http import JsonResponse

def home(request):
    return render(request, 'main/home.html', {'form':FormSaran, 'saran':Saran.objects.all()})

def savesaran(request):
	saran = FormSaran(request.POST or None)
	if saran.is_valid() and request.method == 'POST':
		if request.user.is_authenticated:
			saran.save()
	return redirect('main:home')

def deletesaran(request, pk):
	saran = Saran.objects.get(id=pk)
	if request.method == 'POST':
		saran.delete()
	return redirect('main:home')

def like(request):
	if request.method == 'GET':
		saran_id = request.GET['saran_id']
		likedsaran = Saran.objects.get(pk=saran_id) #getting the liked posts
		m = Like(saran_lk=likedsaran) # Creating Like Object
		m.save()  # saving it to store in database
		return HttpResponse("Success!") # Sending an success response

def cari(request):
	return render(request, 'main/cari.html')

def data(request):
	isi = request.GET['q']
	saran_all = Saran.objects.all()
	data = []
	for sr in saran_all:
		nm = sr.nama
		loc = sr.lokasi
		if (isi.lower() in nm.lower()) or (isi.lower() in loc.lower()):
			data.append([nm, loc, sr.saran])
	return JsonResponse(data=data, safe=False)