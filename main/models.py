from django.db import models

class Saran(models.Model):
	nama = models.CharField(max_length=50, null=True, blank=True)
	lokasi = models.CharField(max_length=50, null=True, blank=True)
	saran = models.TextField()

class Like(models.Model):
    saran_lk = models.ForeignKey(Saran, on_delete=models.CASCADE)