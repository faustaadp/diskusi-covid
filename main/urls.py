from django.urls import path

from . import views
from .views import savesaran, deletesaran

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('savesaran', savesaran, name='savesaran'),
    path('deletesaran/<str:pk>', deletesaran, name='deletesaran'),
    path('like/', views.like, name='like'),
    path('cari/', views.cari, name='cari'),
    path('data/', views.data, name='data'),
]
