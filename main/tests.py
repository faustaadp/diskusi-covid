from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .models import Saran
from .forms import FormSaran
from .views import home, savesaran, deletesaran
from django.contrib.auth.models import User
import json

class MainTestCase(TestCase):
    def setUp(self):
        saran = Saran.objects.create(nama="Sakura", lokasi="Depok", saran="test1")

    def test_url_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_views_use_saran(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_cari(self):
        response = self.client.get('/cari/')
        self.assertEqual(response.status_code, 200)

    def test_data_1(self):
        response = self.client.get('/data/?q=sakura')
        response_json = json.loads(response.content.decode())
        self.assertEqual(response.status_code, 200)
        self.assertEquals(response_json, [["Sakura", "Depok", "test1"]])
    
    def test_data_2(self):
        response = self.client.get('/data/?q=depok')
        response_json = json.loads(response.content.decode())
        self.assertEqual(response.status_code, 200)
        self.assertEquals(response_json, [["Sakura", "Depok", "test1"]])

class Login(TestCase):
    def setUp(self):
        user = User.objects.create_user(username="user01", password="katasandi01")
        data = {'username':'user01', 'password':'katasandi01'}
        response = self.client.post('/login/', data)
        saran = Saran.objects.create(nama="Sakura", lokasi="Depok", saran="test1")
    
    def test_save_saran(self):
        response = self.client.post('/savesaran', data={'nama':'Sakura','lokasi':'Depok','saran':'test2'})
        self.assertEqual(Saran.objects.all().count(), 2)
        self.assertEqual(Saran.objects.get(id=2).nama, "Sakura")
        self.assertEqual(Saran.objects.get(id=2).lokasi, "Depok")
        self.assertEqual(Saran.objects.get(id=2).saran, "test2")

    def test_tampilan_setelah_login(self):
        response = self.client.get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn("LOG IN UNTUK MENYAMPAIKAN SARAN", html_kembalian)

    def test_delete_saran(self):
        response = self.client.post('/deletesaran/1')
        self.assertEqual(Saran.objects.all().count(), 0)
    
    def test_like_saran(self):
        response = self.client.get('/like/?saran_id=1')
        self.assertEqual(response.status_code, 200)


class TanpaLogin(TestCase):
    def test_save_saran(self):
        response = self.client.post('/savesaran', data={'nama':'Sakura','lokasi':'Depok','saran':'test2'})
        self.assertEqual(Saran.objects.all().count(), 0)

    def test_tampilan_tidak_login(self):
        response = self.client.get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("LOG IN UNTUK MENYAMPAIKAN SARAN", html_kembalian)