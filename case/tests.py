from django.test import LiveServerTestCase, TestCase, tag, Client
from django.contrib.auth.models import User
from django.urls import reverse
from selenium import webdriver
from .models import Kasus
from .forms import FormKasus
from .views import case, detail
import json

class MainTestCase(TestCase):
    def setUp(self):
        Kasus.objects.create(jumlah=2, prov="DKI Jakarta", kotkab="Jakarta Pusat")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="jakARta pusaT")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="  jaKarta     puSat   ")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="jakarta pusat ")
        Kasus.objects.create(jumlah=1, prov="DKI Jakarta", kotkab="jakarta pusat")
        Kasus.objects.create(jumlah=15, prov="DIY", kotkab="Yogyakarta")


    def test_model_buat(self):
        self.assertEqual(Kasus.objects.all().count(), 6)
        self.assertEqual(Kasus.objects.get(id=1).prov, "DKI Jakarta")
        self.assertEqual(Kasus.objects.get(id=1).jumlah, 2)
        self.assertEqual(Kasus.objects.get(id=1).kotkab, "Jakarta Pusat")

    def test_cari_kota(self):
        response = Client().get('/case/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Jakarta Pusat", html_kembalian)
        self.assertIn("Yogyakarta", html_kembalian)
        self.assertNotIn("jakARta pusaT", html_kembalian)
        self.assertNotIn("  jaKarta     puSat   ", html_kembalian)
        self.assertNotIn("jakarta pusat ", html_kembalian)
        self.assertNotIn("jakarta pusat", html_kembalian)
        self.assertIn("DKI Jakarta : 6", html_kembalian)
        self.assertIn("DIY : 15", html_kembalian)

    def test_detail_cari_kota(self):
        response = Client().get('/case/detail/1')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Jakarta Pusat", html_kembalian)
        self.assertIn("DKI Jakarta", html_kembalian)
        self.assertIn("6 Kasus", html_kembalian)
        self.assertNotIn("jakARta pusaT", html_kembalian)
        self.assertNotIn("  jaKarta     puSat   ", html_kembalian)
        self.assertNotIn("jakarta pusat ", html_kembalian)
        self.assertNotIn("jakarta pusat", html_kembalian)

    def test_cari(self):
        response = self.client.get('/case/cari/')
        self.assertEqual(response.status_code, 200)

    def test_data_1(self):
        response = self.client.get('/case/data/?q=jakarta')
        response_json = json.loads(response.content.decode())
        self.assertEqual(response.status_code, 200)
        self.assertEquals(response_json, [[6, 'Jakarta Pusat', 'DKI Jakarta']])

    def test_data_2(self):
        response = self.client.get('/case/data/?q=diy')
        response_json = json.loads(response.content.decode())
        self.assertEqual(response.status_code, 200)
        self.assertEquals(response_json, [[15, 'Yogyakarta', 'DIY']])


class Login(TestCase):
    def setUp(self):
        user = User.objects.create_user(username="user01", password="katasandi01")
        data = {'username':'user01', 'password':'katasandi01'}
        response = self.client.post('/login/', data)

    def test_post_case(self):
        response = self.client.post("/case/", data={"jumlah": "2", "prov": "DKI Jakarta", "kotkab": "Jakarta Pusat"})
        self.assertEqual(Kasus.objects.all().count(), 1)

    def test_tampilan(self):
        response = self.client.get('/case/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("SAMPAIKAN DISINI", html_kembalian)
        self.assertNotIn("LOG IN UNTUK SAMPAIKAN", html_kembalian)

class TanpaLogin(TestCase):
    def test_post_case(self):
        response = self.client.post("/case/", data={"jumlah": "2", "prov": "DKI Jakarta", "kotkab": "Jakarta Pusat"})
        self.assertEqual(Kasus.objects.all().count(), 0)

    def test_tampilan(self):
        response = self.client.get('/case/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("LOG IN UNTUK SAMPAIKAN", html_kembalian)
        self.assertNotIn("SAMPAIKAN DISINI", html_kembalian)