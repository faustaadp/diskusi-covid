from django.db import models

class Kasus(models.Model):
	jumlah = models.PositiveIntegerField(default = 1)
	prov = models.CharField(max_length = 50, choices= [('Aceh', 'Aceh'), ('Bali', 'Bali'), ('Banten', 'Banten'), ('Bengkulu', 'Bengkulu'), ('DIY', 'DIY'), ('DKI Jakarta', 'DKI Jakarta'), ('Gorontalo', 'Gorontalo'), ('Jambi', 'Jambi'), ('Jabar', 'Jabar'), ('Jateng', 'Jateng'), ('Jatim', 'Jatim'), ('Kalbar', 'Kalbar'), ('Kalsel', 'Kalsel'), ('Kalteng', 'Kalteng'), ('Kaltim', 'Kaltim'), ('Kaltara', 'Kaltara'), ('Babel', 'Babel'), ('Kepri', 'Kepri'), ('Lampung', 'Lampung'), ('Maluku', 'Maluku'), ('Malut', 'Malut'), ('NTB', 'NTB'), ('NTT', 'NTT'), ('Papua', 'Papua'), ('Papbar', 'Papbar'), ('Riau', 'Riau'), ('Sulbar', 'Sulbar'), ('Sulsel', 'Sulsel'), ('Sulteng', 'Sulteng'), ('Sultara', 'Sultara'), ('Sulut', 'Sulut'), ('Sumbar', 'Sumbar'), ('Sumsel', 'Sumsel'), ('Sumut', 'Sumut')])
	kotkab = models.CharField(max_length = 50)
