from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.http import JsonResponse
from .models import Kasus
from .forms import FormKasus

def case(request):
	form = FormKasus()
	if request.method == "POST":
		form = FormKasus(request.POST)
		if form.is_valid() and request.user.is_authenticated:
			form.save()
			return redirect('case:case')
	dct = {}
	prov_list = []
	prov_all = []
	kasus = Kasus.objects.all()
	for ks in kasus:
		if ks.prov in dct :
			dct[ks.prov] += ks.jumlah
		else : 
			dct[ks.prov] = ks.jumlah
			prov_list.append(ks.prov)	
	for prov in prov_list :
		tmp = []
		for ks in kasus:
			if ks.prov in prov:
				s = ' '.join(w.capitalize() for w in ks.kotkab.split())    	
				if not s in tmp:
					tmp.append(s)
		tmp2 = []
		tmp2.append(prov)
		tmp2.append(tmp)
		tmp2.append(dct[prov])
		prov_all.append(tmp2)
		for ks in kasus:
			if ks.prov in prov:
				tmp2.append(ks)
				break
	context = {'form' : form, 'prov_all' : prov_all}
	return render(request, 'case/case.html', context)

def detail(request, nomor):
	prov = Kasus.objects.get(id = nomor)
	kasus_all = Kasus.objects.all()
	dct = {}
	data = []
	jum = 0
	for ks in kasus_all:
		if ks.prov == prov.prov:
			jum += ks.jumlah
			s = ' '.join(w.capitalize() for w in ks.kotkab.split())    	
			if s in dct:
				dct[s] += ks.jumlah
			else:
				dct[s] = ks.jumlah
	data.append(['Kota/Kabupaten', 'Jumlah'])
	for ks in dct:
		data.append([ks, dct[ks]])
	context = {'prov': prov, 'data': data, 'jum': jum}
	return render(request, 'case/detail.html', context)

def cari(request):
	return render(request, 'case/cari.html')

def data(request):
	isi = request.GET['q']
	isi = ' '.join(w.capitalize() for w in isi.split())
	kasus_all = Kasus.objects.all()
	dct = {}
	dctp = {}
	data = []
	for ks in kasus_all:
		kk = ' '.join(w.capitalize() for w in ks.kotkab.split())    	
		pp = ' '.join(w.capitalize() for w in ks.prov.split())  
		if (isi in kk) or (isi in pp):
			if kk in dct:
				dct[kk] += ks.jumlah
			else:
				dctp[kk] = ks.prov
				dct[kk] = ks.jumlah
	for ks in dct:
		data.append([dct[ks], ks, dctp[ks]])
	data.sort(reverse = True)
	return JsonResponse(data=data, safe=False)