from django.urls import path

from . import views

app_name = 'case'

urlpatterns = [
    path('', views.case, name='case'),
    path('detail/<int:nomor>', views.detail, name='detail'),
    path('cari/', views.cari, name='cari'),
    path('data/', views.data, name='data'),
]
