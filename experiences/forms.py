from django import forms
from .models import Experience

class FormExperience(forms.ModelForm):
    class Meta:
        model = Experience
        fields = "__all__"
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
            'lokasi' : forms.TextInput(attrs={'class': 'form-control'}),
            'pengalaman' : forms.Textarea(attrs={'class': 'form-control'}),
        }
