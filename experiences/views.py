from django.shortcuts import render, redirect
from .forms import FormExperience
from .models import Experience
from login.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core import serializers

# Create your views here.
def experiences(request):
    return render(request, 'experiences/experiences.html', {'experiences' : Experience.objects.all()})
    
@login_required(login_url='login:login')
def cariexperiences(request):
    return render(request, 'experiences/cariexperiences.html')

@login_required(login_url='login:login')
def dataexperiences(request):
    param = request.GET['q']
    param = param.capitalize()
    hasil = []
    key = 0
    experience_all = Experience.objects.all()

    for experience in experience_all:
        username = experience.profile.user.username
        kab_kota = experience.profile.kota_kabupaten
        provinsi = experience.profile.provinsi
        pengalaman = experience.pengalaman

        dataex = {'username':username, 'kota_kabupaten':kab_kota, 'provinsi':provinsi, 'pengalaman':pengalaman, }

        if (param in username.capitalize()) or (param in kab_kota.capitalize()) or (param in provinsi.capitalize()) or (param in pengalaman.capitalize()):
            hasil.append(dataex)
            key += 1

    return JsonResponse(hasil, safe=False)

@login_required(login_url='login:login')
def saveexperience(request): 
    experience = request.POST['pengalaman']
    if request.user.is_authenticated and request.method == 'POST':
        profile = request.user.profile
        new = Experience(profile=profile, pengalaman=experience)
        new.save()
        experience = {'id': new.id , 'username': new.profile.user.username, 'provinsi' : new.profile.provinsi, 'kota_kabupaten' : new.profile.kota_kabupaten, 'pengalaman' : new.pengalaman, }
        data = {
            "experience" : experience,
        }

        return JsonResponse(data)    
    


@login_required(login_url='login:login')
def deleteexperience(request, pk):
    experience = Experience.objects.get(id=pk)
    if request.method == 'POST':
        experience.delete()

    return redirect('experiences:experiences')

