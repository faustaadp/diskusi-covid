$(document).ready(function () {

    $("#saveexperience").submit(function (e) { 
        e.preventDefault();
        var action = confirm("Apakah data yang anda masukkan sudah sesuai?")
        
        if (action != false) {
            $.ajax({
                type: "POST",
                url: "saveexperience",
                data: {
                    pengalaman : $("#pengalaman").val(),
                    csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
                },
                success: function (response) {
                    $("#daftar").append(
                    '<div class="card border-white"><div class="card-body"><h4 class="card-title">' + response.experience.username + 
                    '</h4><p class="card-subtitle mb-3 text-muted">' + response.experience.kota_kabupaten + ', ' + response.experience.provinsi + 
                    '</p><h5 class="card-text">' + response.experience.pengalaman + '</h5>' + 
                    '<div class="modal fade" id="exampleModal' + response.experience.id+ '" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                        '<div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>' + 
                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'+
                            
                            '<div class="modal-body"><h4>Apakah anda ingin menghapus tanggapan ini?</h4><div class="card border-white"><div class="card-body"><h4 class="card-title">' + 
                            response.experience.username + '</h4><p class="card-subtitle mb-3 text-muted">' + response.experience.kota_kabupaten + ',' + response.experience.provinsi + 
                            '</p><h5 class="card-text">' + response.experience.pengalaman + '</h5></div></div></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button' +

                            '<form class="text-center" action="' + response.delete + '" method="POST"><button type="submit" class="btn btn-danger">Hapus</button></form></div></div></div></div>'
                        
                    );
                }
            });

        }
        $('#saveexperience').trigger("reset");
    });
});