from django.urls import path

from .views import experiences, saveexperience, deleteexperience, cariexperiences, dataexperiences

app_name = 'experiences'

urlpatterns = [
    path('', experiences, name='experiences'),
    path('saveexperience', saveexperience, name='saveexperience' ),
    path('deleteexperience/<str:pk>', deleteexperience, name='deleteexperience'),
    path('cari', cariexperiences, name='cari'),
    path('data', dataexperiences, name='data'),

]
