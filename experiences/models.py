from django.db import models
from login.models import Profile

# Create your models here.
class Experience(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    pengalaman = models.TextField()