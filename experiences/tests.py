from django.test import TestCase
from django.urls import resolve
from .views import experiences, saveexperience, deleteexperience
from .models import Experience, Profile
from django.contrib.auth.models import User
import json

# Create your tests here.
class ExperiencesTest(TestCase):
    def setUp(self):
        data = {'username':'user01', 'password1':'katasandi01', 'password2':'katasandi01', 'provinsi':'Aceh', 'kota_kabupaten':'Banda Aceh'}
        response = self.client.post('/signup/', data=data)
        self.client.login(username='user01', password='katasandi01')
        profile = Profile.objects.get(id=1)
        experience = Experience.objects.create(profile=profile, pengalaman="test1")

    def test_url_experiences(self):
        response = self.client.get('/experiences/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'experiences/experiences.html')

    
    def test_views_use_experiences(self):
        found = resolve('/experiences/')
        self.assertEqual(found.func, experiences)

    def test_save_experience(self):
        response = self.client.post('/experiences/saveexperience', data={ 'pengalaman' : 'test2'})
        self.assertEqual(Experience.objects.all().count(), 2)
        self.assertEqual(Experience.objects.get(id=2).profile.user.username, "user01")
        self.assertEqual(Experience.objects.get(id=2).profile.provinsi, "Aceh")
        self.assertEqual(Experience.objects.get(id=2).profile.kota_kabupaten, "Banda Aceh")
        self.assertEqual(Experience.objects.get(id=2).pengalaman, "test2")

    def test_delete_experience(self):
        response = self.client.post('/experiences/deleteexperience/1')
        self.assertEqual(Experience.objects.all().count(), 0)
    
    def test_data(self):
        response = self.client.get('/experiences/data?q=Aceh')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), [{'username':'user01', 'kota_kabupaten':'Banda Aceh', 'provinsi':'Aceh', 'pengalaman':'test1', }])
    
    def test_cari(self):
        response = self.client.get('/experiences/cari')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'experiences/cariexperiences.html')

class TanpaLogin(TestCase):

    def test_tampilan(self):
        response = self.client.get('/experiences/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("LOG IN UNTUK CARI & SAMPAIKAN", html_kembalian)
        self.assertNotIn("SAMPAIKAN DISINI", html_kembalian)
    
    def test_post_case(self):
        response = self.client.post("/experience/saveexperience", data={"pengalaman": "Tes"})
        self.assertEqual(Experience.objects.all().count(), 0)