from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    provinsi = models.CharField(max_length = 50, choices= [
        ('Aceh', 'Aceh'), 
        ('Bali', 'Bali'), 
        ('Banten', 'Banten'), 
        ('Bengkulu', 'Bengkulu'), 
        ('DIY', 'DIY'), 
        ('DKI Jakarta', 'DKI Jakarta'), 
        ('Gorontalo', 'Gorontalo'), 
        ('Jambi', 'Jambi'), 
        ('Jawa Barat', 'Jawa Barat'), 
        ('Jawa Tengah', 'Jawa Tengah'), 
        ('Jawa Timur', 'Jawa Timur'), 
        ('Kalimantan Barat', 'Kalimantan Barat'),
        ('Kalimantan Selatan', 'Kalimantan Selatan'), 
        ('Kalimantan tengah', 'Kalimantan tengah'), 
        ('Kalimantan Timur', 'Kalimantan Timur'), 
        ('Kalimantan Utara', 'Kalimantan Utara'), 
        ('Kep Bangka Belitung', 'Kep Bangka Belitung'), 
        ('Kep Riau', 'Kep Riau'), 
        ('Lampung', 'Lampung'), 
        ('Maluku', 'Maluku'), 
        ('Maluku Utara', 'Maluku Utara'), 
        ('NTB', 'NTB'), 
        ('NTT', 'NTT'), 
        ('Papua', 'Papua'), 
        ('Papua Barat', 'Papua Barat'), 
        ('Riau', 'Riau'), 
        ('Sulawesi Barat', 'Sulawesi Barat'), 
        ('Sulawesi Selatan', 'Sulawesi Selatan'), 
        ('Sulawesi Tengah', 'Sulawesi Tengah'), 
        ('Sulawesi Tenggara', 'Sulawesi Tenggara'), 
        ('Sulawesi Utara', 'Sulawesi Utara'), 
        ('Sumatera Barat', 'Sumatera Barat'), 
        ('Sumatera Selatan', 'Sumatera Selatan'), 
        ('Sumatera Utara', 'Sumatera Utara')], 
        default = 'Aceh')
    kota_kabupaten = models.CharField(max_length = 50, blank=True)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()