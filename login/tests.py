from django.test import TestCase, SimpleTestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .views import log_in, log_out, signup
from django.urls import resolve

# Create your tests here.
class MainTestCase(TestCase):
	def test_url_login(self):
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_url_logout(self):
		response = self.client.get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_url_signup(self):
		response = self.client.get('/signup/')
		self.assertEqual(response.status_code, 200)

	def test_new_user_created_and_redirected(self):
		data = {'username':'user01', 'password1':'katasandi01', 'password2':'katasandi01', 'provinsi':'Aceh', 'kota_kabupaten':'Banda Aceh'}
		response = self.client.post('/signup/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_template_signup(self):
		response = Client().get('/signup/')
		self.assertTemplateUsed(response, 'login/signup.html')

	def test_login(self):
		self.client.post('/signup/' , data={'username' : 'user01', 'password' : 'pass01', 'provinsi':'Aceh', 'kota_kabupaten':'Banda Aceh'})
		response = self.client.post('/login/' , data={'username' : 'user01', 'password' : 'pass01'}) 
		self.assertEqual(response.status_code, 302)
		self.assertRedirects(response, '/login/')

	def test_user_login_success_and_redirected(self):
		user = User.objects.create_user(username="user01", password="katasandi01")
		data = {'username':'user01', 'password':'katasandi01'}
		response = self.client.post('/login/', data)
		self.assertRedirects(response, '/')

	def test_login_salah(self):
	    self.client.post('/signup/' , data={'username' : 'user01', 'password' : 'pass01', 'provinsi':'Aceh', 'kota_kabupaten':'Banda Aceh'})
	    response = self.client.post('/login/' , data={'username' : 'user01', 'password' : 'pass02'}) 
	    response = self.client.get('/login/')
	    html_kembalian = response.content.decode('utf8')
	    self.assertIn("Username / Password salah", html_kembalian)
	    self.assertEqual(response.status_code, 200)

	def test_navbar_tidak_login(self):
	 	response = self.client.get('/')
	 	html_kembalian = response.content.decode('utf8')
	 	self.assertIn("Log in", html_kembalian)
	 	self.assertIn("Sign Up", html_kembalian)
    
	#def test_navbar_login(self):
	#	self.client.post('/signup/' , data={'username' : 'user01', 'password' : 'pass01', 'provinsi':'Aceh', 'kota_kabupaten':'Banda Aceh'})
	#	self.client.post('/login/', data={'username' : 'user01', 'password' : 'pass01'})
	#	
	#	html_kembalian = response.content.decode('utf8')
	#	
	#	self.assertIn("Log out", html_kembalian)
