from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import SignUpForm
# Create your views here.
def log_in(request):
	if request.method == "POST":
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username = username, password = password)
		if user is not None:
			if user.is_active:
				login(request, user)	
			return redirect('main:home')
		else:
			messages.error(request, 'Username / Password salah')
			return redirect('login:log_in')
	return render(request, 'login/login.html')

def log_out(request):
	logout(request)
	return redirect('main:home')

def signup(request):
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		if form.is_valid():
			user = form.save()
			user.refresh_from_db()  # load the profile instance created by the signal
			user.profile.provinsi = form.cleaned_data.get('provinsi')
			user.profile.kota_kabupaten = form.cleaned_data.get('kota_kabupaten')
			user.save()
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=user.username, password=raw_password)
			login(request, user)
			return redirect('login:log_in')
	else:
		form = SignUpForm()
	return render(request, 'login/signup.html', {'form': form})